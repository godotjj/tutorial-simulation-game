extends TileMap

## Distance from the player when the mouse stops being able to interact.
const MAXIMUM_WORK_DISTANCE := 275.0

## Base time in seconds it takes to deconstruct an item.
const DECONSTRUCT_TIME := 0.3

const LAYER := 0

var GroundItemScene := preload("res://Entities/GroundItem.tscn")
var _gui: Control

## The simulation's entity tracker. We use its functions to know if a cell is available or it
## already has an entity.
var _tracker: EntityTracker

## The ground tiles. We can check the position we're trying to put an entity down on
## to see if the mouse is over the tilemap.
var _ground: TileMap

var _flat_entities: Node2D

## The player entity. We can use it to check the distance from the mouse to prevent
## the player from interacting with entities that are too far away.
var _player: CharacterBody2D

## The variable below keeps track of the current deconstruction target cell. If the mouse moves
## to another cell, we can abort the operation by checking against this value.
var _current_deconstruct_location := Vector2i.ZERO

@onready var _deconstruct_timer := $Timer


func _process(_delta: float) -> void:
	var has_placeable_blueprint: bool = _gui.blueprint and _gui.blueprint.placeable
	# If we have a blueprint in hand, keep it updated and snapped to the grid
	if has_placeable_blueprint and not _gui.mouse_in_gui:
		_move_blueprint_in_world(local_to_map(get_global_mouse_position()))


## Here's our setup() function. It sets the placer up with the data that it needs to function,
## and adds any pre-placed entities to the tracker.
func setup(
	gui: Control,
	tracker: EntityTracker,
	ground: TileMap,
	flat_entities: Node2D,
	player: CharacterBody2D
) -> void:
	# We use the function to initialize our private references. As mentioned before, this approach
	# makes refactoring easier, as the EntityPlacer doesn't need hard-coded paths to the EntityTracker,
	# GroundTiles, and Player nodes.
	_gui = gui
	_tracker = tracker
	_ground = ground
	_flat_entities = flat_entities
	_player = player

	# For each child of EntityPlacer, if it extends Entity, add it to the tracker
	# and ensure its position snaps to the isometric grid.
	for child in get_children():
		if not child is Entity:
			return

		# Get the world position of the child into map coordinates. These are
		# integer coordinates, which makes them ideal for repeatable
		# Dictionary keys, instead of the more rounding-error prone
		# decimal numbers of world coordinates.
		var map_position := local_to_map(child.global_position)

		# Report the entity to the tracker to add it to the dictionary.
		_tracker.place_entity(child, map_position)


# Below, we start by storing the result of calculations and comparisons in variables. Doing so makes
# the code easy to read.
func _unhandled_input(event: InputEvent) -> void:
	# Get the mouse position in world coordinates relative to world entities.
	# event.global_position and event.position return mouse positions relative
	# to the screen, but we have a camera that can move around the world.
	# That's why we call `get_global_mouse_position()`
	var global_mouse_position := get_global_mouse_position()

	# We check whether we have a blueprint in hand and that the player can place it in the world.
	var has_placeable_blueprint: bool = _gui.blueprint and _gui.blueprint.placeable

	# We check if the mouse is close enough to the Player node.
	var is_close_to_player := (
		global_mouse_position.distance_to(_player.global_position) < MAXIMUM_WORK_DISTANCE
	)

	# Here, we calculate the coordinates of the cell the mouse is hovering.
	var cellv := local_to_map(global_mouse_position)

	# We check whether an entity exists at that map coordinate or not, to not
	# add entities in occupied cells.
	var cell_is_occupied := _tracker.is_cell_occupied(cellv)

	# We check whether there is a ground tile underneath the current map coordinates.
	# We don't want to place entities out in the air.
	var is_on_ground: bool = _ground.get_cell_source_id(LAYER, cellv) == 0

	# If the user releases right-click or clicks another mouse button, we can abort.
	# We catch all mouse button inputs here because the `_abort_deconstruct()` function
	# below will safely disconnect the timer and stop it.
	if event is InputEventMouseButton:
		_abort_deconstruct()

	# When left-clicking, we use all our boolean variables to check the player can place an entity.
	# Using variables with clear names helps to write code that reads *almost* like English.
	if event.is_action_pressed("left_click"):
		if has_placeable_blueprint:
			if not cell_is_occupied and is_close_to_player and is_on_ground:
				_place_entity(cellv)
				_update_neighboring_flat_entities(cellv)

	# When right clicking...
	elif event.is_action_pressed("right_click") and not has_placeable_blueprint:
		# ...onto a tile within range that has an entity in it,
		if cell_is_occupied and is_close_to_player:
			# we remove that entity.
			_deconstruct(global_mouse_position, cellv)

	elif event is InputEventMouseMotion:
		# If the mouse moves and slips off the target tile, then we can abort
		# the deconstruction process.
		if cellv != _current_deconstruct_location:
			_abort_deconstruct()

	# If the mouse moved and we have a blueprint in hand, we update the blueprint's ghost so it
	# follows the mouse cursor.
	elif event is InputEventMouseMotion:
		if has_placeable_blueprint:
			_move_blueprint_in_world(cellv)

	# When the user presses the drop button and we are holding a blueprint, we would
	# drop the entity as a dropable entity that the player can pick up.
	# For testing purposes, the following code clears the blueprint from the active slot instead.
	elif event.is_action_pressed("drop") and _gui.blueprint:
		if is_on_ground:
			_drop_entity(_gui.blueprint, global_mouse_position)
			_gui.blueprint = null

	elif event.is_action_pressed("rotate_blueprint") and _gui.blueprint:
		_gui.blueprint.rotate_blueprint()


## Moves the active blueprint in the world according to mouse movement,
## and tints the blueprint based on whether the tile is valid.
func _move_blueprint_in_world(cellv: Vector2i) -> void:
	# Set the blueprint's position and scale back to origin
	_gui.blueprint.display_as_world_entity()

	# Snap the blueprint's position to the mouse, because the Blueprint is now a child of a CanvasLayer, we need to use
	# the viewport origin to sum it to the mapped position
	_gui.blueprint.global_position = (map_to_local(cellv) + get_viewport_transform().get_origin())

	# Determine each of the placeable conditions
	var is_close_to_player := (
		get_global_mouse_position().distance_to(_player.global_position) < MAXIMUM_WORK_DISTANCE
	)

	var is_on_ground: bool = _ground.get_cell_source_id(LAYER, cellv) == 0
	var cell_is_occupied := _tracker.is_cell_occupied(cellv)

	# Tint according to whether the current tile is valid or not.
	if not cell_is_occupied and is_close_to_player and is_on_ground:
		_gui.blueprint.modulate = Color.WHITE
	else:
		_gui.blueprint.modulate = Color.RED

	if _gui.blueprint is WireBlueprint:
		WireBlueprint.set_sprite_for_direction(_gui.blueprint.sprite, _get_powered_neighbors(cellv))


## Places the entity corresponding to the active `_gui.blueprint` in the world at the specified
## location, and informs the `EntityTracker`.
func _place_entity(cellv: Vector2i) -> void:
	# We get the blueprint's name using the blueprint and use it to get an instance
	# of the corresponding entity.
	var entity_name := Library.get_entity_name_from(_gui.blueprint)
	var new_entity: Node2D = Library.entities[entity_name].instantiate()

	if _gui.blueprint is WireBlueprint:
		var directions := _get_powered_neighbors(cellv)
		_flat_entities.add_child(new_entity)
		WireBlueprint.set_sprite_for_direction(new_entity.sprite, directions)
	else:
		add_child(new_entity)

	# Snap its position to the map
	new_entity.global_position = map_to_local(cellv)

	# Call `setup()` on the entity so it can use any data the blueprint holds to configure itself.
	new_entity._setup(_gui.blueprint)

	# Register the new entity in the `EntityTracker` so all the signals can go up, as with systems.
	_tracker.place_entity(new_entity, cellv)

	if _gui.blueprint.stack_count == 1:
		_gui.destroy_blueprint()
	else:
		_gui.blueprint.stack_count -= 1
		_gui.update_label()


## Begin the deconstruction process at the current cell
func _deconstruct(event_position: Vector2i, cellv: Vector2i) -> void:
	# We connect to the timer's `timeout` signal. We pass in the targeted tile as a
	# bind argument and make sure that the signal disconnects after emitting once
	# using the CONNECT_ONE_SHOT flag. This is because once the signal has triggered,
	# we do not want to have to disconnect manually. Once the timer ends, the deconstruct
	# operation ends.
	_deconstruct_timer.timeout.connect(_finish_deconstruct.bind(cellv), CONNECT_ONE_SHOT)

	# We then start the timer and store the cell we're targeting, which allows us to cancel
	# the operation if the player's mouse moves to another cell.
	_deconstruct_timer.start(DECONSTRUCT_TIME)
	_current_deconstruct_location = cellv


## Finish the deconstruction and delete the entity from the game world.
func _finish_deconstruct(cellv: Vector2i) -> void:
	# This function will drop the deconstructed entity as a pickup item,
	# but we haven't implemented an inventory yet, so we only remove the entity.
	var entity := _tracker.get_entity_at(cellv)
	# Get the entity's name so we can check if we have access to a blueprint.
	var entity_name := Library.get_entity_name_from(entity)
	# We convert the map position to a global position.
	var location := map_to_local(cellv)

	# If we do have a blueprint, we get it as a packed scene.
	if Library.blueprints.has(entity_name):
		var Blueprint: PackedScene = Library.blueprints[entity_name]
		_drop_entity(Blueprint.instantiate(), location)

	_tracker.remove_entity(cellv)
	_update_neighboring_flat_entities(cellv)


## Disconnect from the timer if connected, and stop it from continuing, to prevent
## deconstruction from completing.
func _abort_deconstruct() -> void:
	if _deconstruct_timer.timeout.is_connected(_finish_deconstruct):
		_deconstruct_timer.timeout.disconnect(_finish_deconstruct)

	_deconstruct_timer.stop()


## Returns a bit-wise integer based on whether the nearby objects can carry power.
func _get_powered_neighbors(cellv: Vector2i) -> int:
	# Begin with a blank direction of 0
	var direction := 0
	var neighbouring_cells := _get_neighbouring_cells(cellv)

	# We loop over each neighboring direction from our `neighbors` dictionary.
	for neighbor_cell in neighbouring_cells.keys():
		# We calculate the neighbor cell's coordinates.
		var key: Vector2i = cellv + neighbouring_cells[neighbor_cell]

		if not _tracker.is_cell_occupied(key):
			continue

		# We get the entity in that cell if there is one.
		var entity: Node = _tracker.get_entity_at(key)

		# If the entity is part of any of the power groups,
		if (
			entity.is_in_group(Types.POWER_MOVERS)
			or entity.is_in_group(Types.POWER_RECEIVERS)
			or entity.is_in_group(Types.POWER_SOURCES)
		):
			# We combine the number with the OR bitwise operator.
			# It's like using +=, but | prevents the same number from adding to itself.
			# Types.Direction.RIGHT (1) + Types.Direction.RIGHT (1) results in DOWN (2), which is wrong.
			# Types.Direction.RIGHT (1) | Types.Direction.RIGHT (1) still results in RIGHT (1).
			# Since we are iterating over all four directions and will not repeat, you can use +,
			# but I use the | operator to be more explicit about comparing bitwise enum FLAGS.
			direction |= neighbor_cell

	return direction


## Looks at each of the neighboring tiles and updates each of them to use the
## correct graphics based on their own neighbors.
func _update_neighboring_flat_entities(cellv: Vector2i) -> void:
	var neighbouring_cells := _get_neighbouring_cells(cellv)

	# For each neighboring tile,
	for neighbor_cell in neighbouring_cells.keys():
		# We get the entity, if there is one
		var key: Vector2i = cellv + neighbouring_cells[neighbor_cell]
		var object = _tracker.get_entity_at(key)

		# If it's a wire, we have that wire update its graphics to connect to the new
		# entity.
		if object and object is WireEntity:
			var tile_directions := _get_powered_neighbors(key)
			WireBlueprint.set_sprite_for_direction(object.sprite, tile_directions)


func _get_neighbouring_cells(cellv: Vector2i) -> Dictionary:
	if cellv.y % 2 == 0:
		return Types.NEIGHBORS_EVEN_Y

	return Types.NEIGHBORS_ODD_Y


## Creates a new ground item with the given blueprint and sets it up at the
## deconstructed entity's location.
func _drop_entity(entity: BlueprintEntity, location: Vector2) -> void:
	# We instance a new ground item, add it, and set it up
	var ground_item: GroundItem = GroundItemScene.instantiate()
	add_child(ground_item)
	ground_item.setup(entity, location)
