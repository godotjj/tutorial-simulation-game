extends Entity

## The total amount of power the battery can hold at max capacity.
@export var max_storage := 1000.0

## The amount of power the battery is currently holding.
## See the setter function below to see how we use it to update the source and receiver
## components' efficiency.
var stored_power := 0.0:
	set(value):
		stored_power = value
		await _changed_stored_power()
	get:
		return stored_power

## References to our `PowerReceiver` and `PowerSource` nodes.
@onready var receiver: PowerReceiver = $PowerReceiver
@onready var source: PowerSource = $PowerSource
@onready var indicator: Sprite2D = $Indicator


func _ready() -> void:
	# If the source is not omnidirectional:
	if source.output_direction != 15:
		# Set the receiver direction to the _opposite_ of the source.
		# The ^ is the XOR (exclusive or) operator.
		# If | returns 1 if either bit is 1, and & returns 1 if both bits are 1,
		# ^ returns 1 if the bits _do not_ match.

		# This inverts the direction flags, making each direction that's not an output
		# a valid input to receive energy.
		receiver.input_direction = 15 ^ source.output_direction


## The setup function fetches the direction from the blueprint, applies it
## to the source, and inverts it for the receiver with the XOR operator (^).
func _setup(blueprint: BlueprintEntity) -> void:
	source.output_direction = blueprint.power_direction.output_directions
	receiver.input_direction = 15 ^ source.output_direction


## Set the efficiency in source and receiver based on the amount of stored power.
func _changed_stored_power() -> float:
	# We set the stored power and prevent it from becoming negative.
	var new_stored_power = max(stored_power, 0)

	# Wait until the entity is ready to ensure we have access to the `receiver` and the `source` nodes.
	if not is_inside_tree():
		await ready

	# Set the receiver's efficiency.
	receiver.efficiency = (
		0.0
		# If the battery is full, set it to 0. We don't want it to draw more power.
		if new_stored_power >= max_storage
		# If the battery is less than full, set it to between 1 and
		# the percentage of how empty the battery is.
		# This makes the battery fill up slower as it approaches being full.
		else min((max_storage - new_stored_power) / receiver.power_required, 1.0)
	)

	# Set the source efficiency to `0` if there is no power. Otherwise, we set it to a percentage of how full
	# the battery is. A battery that has more power than it must provide returns one, whereas a battery
	# that has less returns some percentage of that.
	source.efficiency = (
		0.0 if new_stored_power <= 0 else min(new_stored_power / source.power_amount, 1.0)
	)
	indicator.material.set("shader_parameter/amount", new_stored_power / max_storage)

	return new_stored_power


## Sets the stored power using the setter based on the received amount of power per second.
func _on_PowerReceiver_received_power(amount: float, delta: float) -> void:
	self.stored_power = stored_power + amount * delta


## Sets the stored power using the setter based on the amount of power provided per second.
func _on_PowerSource_power_updated(power_draw: float, delta: float) -> void:
	self.stored_power = stored_power - min(power_draw, source.get_effective_power()) * delta
