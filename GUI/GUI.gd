extends CenterContainer

## Each of the action as listed in the input map. We place them in an array so we
## can iterate over each one.
const QUICKBAR_ACTIONS := [
	"quickbar_1",
	"quickbar_2",
	"quickbar_3",
	"quickbar_4",
	"quickbar_5",
	"quickbar_6",
	"quickbar_7",
	"quickbar_8",
	"quickbar_9",
	"quickbar_0"
]

## A reference to the inventory that belongs to the 'mouse'. It is a property
## that gives indirect access to DragPreview's blueprint through its getter
## function. No one needs to know that it is stored outside of the GUI class.
var blueprint: BlueprintEntity:
	set(value):
		await _set_blueprint(value)
	get:
		return _get_blueprint()

## If `true`, it means the mouse is over the `GUI` at the moment.
var mouse_in_gui := false

@onready var player_inventory := $HBoxContainer/InventoryWindow
@onready var quickbar := $MarginContainer/QuickBar
# We'll use it later to reparent the `quickbar` node.
@onready var quickbar_container := $MarginContainer
## We use the reference to the drag preview in the setter and getter functions.
@onready var _drag_preview := $DragPreview

@onready var is_open: bool = $HBoxContainer/InventoryWindow.visible

## The parent container that holds the inventory window
@onready var _gui_rect := $HBoxContainer


func _ready() -> void:
	# Here, we'll set up any GUI systems that require knowledge of the GUI node.
	# We'll define `InventoryWindow.setup()` in the next lesson.
	player_inventory.setup(self)
	quickbar.setup(self)


func _process(_delta: float) -> void:
	var mouse_position := get_global_mouse_position()
	# if the mouse is inside the GUI rect and the GUI is open, set it true.
	mouse_in_gui = is_open and _gui_rect.get_rect().has_point(mouse_position)


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_inventory"):
		if is_open:
			_close_inventories()
		else:
			_open_inventories()
	# If we pressed anything else, we can check against our quickbar actions
	elif event.is_pressed():
		for i in QUICKBAR_ACTIONS.size():
			# If the action matches with one of our quickbar actions, we call
			# a function that simulates a mouse click at its location.
			if InputMap.event_is_action(event, QUICKBAR_ACTIONS[i]):
				_simulate_input(quickbar.panels[i])
				# We break out of the loop, since there cannot be more than one
				# action pressed in the same event. We'd be wasting resources otherwise.
				break


## Forwards the `destroy_blueprint()` call to the drag preview.
func destroy_blueprint() -> void:
	_drag_preview.destroy_blueprint()


## Forwards the `update_label()` call to the drag preview.
func update_label() -> void:
	_drag_preview.update_label()


## Simulates a mouse click at the location of the panel.
func _simulate_input(panel: InventoryPanel) -> void:
	# Create a new InputEventMouseButton and configure it as a left button click.
	var input := InputEventMouseButton.new()
	input.button_index = MOUSE_BUTTON_LEFT
	input.pressed = true

	# Provide it directly to the panel's `_gui_input()` function, as we don't care
	# about the rest of the engine intercepting this event.
	panel._gui_input(input)


## Shows the inventory window, crafting window
func _open_inventories() -> void:
	is_open = true
	player_inventory.visible = true
	player_inventory.claim_quickbar(quickbar)


## Hides the inventory window, crafting window, and any currently open machine GUI
func _close_inventories() -> void:
	is_open = false
	player_inventory.visible = false
	_claim_quickbar()


## Setter that forwards setting the blueprint to `DragPreview.blueprint`.
func _set_blueprint(value: BlueprintEntity) -> void:
	if not is_inside_tree():
		await ready
	_drag_preview.blueprint = value


## Getter that returns the DragPreview's blueprint.
func _get_blueprint() -> BlueprintEntity:
	return _drag_preview.blueprint


## Removes the quickbar from its current parent and puts it back under the
## quickbar's margin container
func _claim_quickbar() -> void:
	quickbar.get_parent().remove_child(quickbar)
	quickbar_container.add_child(quickbar)
