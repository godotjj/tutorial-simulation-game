class_name Types
extends RefCounted

## We store constants for possible directions: up, down, left, and right.
## We'll use bitwise operators to combine them, allowing us to map directions
## to different wire sprites.
## For example, if we combine RIGHT and DOWN below, we will get the number `3`.
## We write these numbers in base 10. If you prefer, you can write them in binary using the
## prefix 0b. For example, 0b0001 is the number `1` in binary, and 0b1000 is the number `8`.
enum Direction { RIGHT = 1, DOWN = 2, LEFT = 4, UP = 8 }

## This dictionary maps our `Direction` values to `Vector2i` coordinates, to loop over neighbors of
## a given cell.
const NEIGHBORS_ODD_Y := {
	Direction.RIGHT: Vector2i(1, 1),
	Direction.DOWN: Vector2i(0, 1),
	Direction.LEFT: Vector2i(0, -1),
	Direction.UP: Vector2i(1, -1)
}

const NEIGHBORS_EVEN_Y := {
	Direction.RIGHT: Vector2i(0, 1),
	Direction.DOWN: Vector2i(-1, 1),
	Direction.LEFT: Vector2i(-1, -1),
	Direction.UP: Vector2i(0, -1)
}

# Group name constants. Storing them as constants help prevent typos.
const POWER_MOVERS := "power_movers"
const POWER_RECEIVERS := "power_receivers"
const POWER_SOURCES := "power_sources"
